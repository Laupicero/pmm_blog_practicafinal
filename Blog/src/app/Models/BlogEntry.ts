//Clase Plantilla
export class BlogEntry{
    public title: String;
    public imgURL: String;
    public link: String;

    constructor(title: String, imgURL: String, link: String ){
        this.title = title;
        this.imgURL = imgURL;
        this.link = link;
    }
}