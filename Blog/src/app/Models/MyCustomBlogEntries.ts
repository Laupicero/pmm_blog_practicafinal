import {BlogEntry} from './BlogEntry';

//Exportaremos esta variable a traves de nuestro servicio
export const myBlogNews_DAO: BlogEntry[] = [

    new BlogEntry(`El primer satélite español, que costó 200 millones de euros, se pierde 8 minutos después del lanzamiento`,
    `https://www.mediterraneodigital.com/images/2020/11/satelite-espana.jpg`,
    `https://www.mediterraneodigital.com/sociedad-y-tiempo-libre/satelite-espanol`),

    new BlogEntry(`La Casa Real comenzará a tributar en España`,
    `https://emtstatic.com/2020/11/Captura-de-pantalla-2020-11-24-a-las-13.53.47-696x463.png`,
    `https://www.elmundotoday.com/2020/11/la-casa-real-comenzara-a-tributar-en-espana/`),

    new BlogEntry(`Un concursante de Pasapalabra pierde el bote pero se lleva la vacuna contra el coronavirus del programa`,
    `https://emtstatic.com/2020/11/Captura-de-pantalla-2020-11-20-a-las-13.04.04-696x419.png`,
    `https://www.elmundotoday.com/2020/11/un-concursante-de-pasapalabra-pierde-el-bote-pero-se-lleva-la-vacuna-contra-el-coronavirus-del-programa/`),

    new BlogEntry(`El Gobierno crea un segundo Ministerio de la Verdad para desmentir las informaciones sobre el Ministerio de la Verdad`,
    `https://emtstatic.com/2020/11/Captura-de-pantalla-2020-11-12-a-las-11.25.29-696x461.png`,
    `https://www.elmundotoday.com/2020/11/el-gobierno-crea-un-segundo-ministerio-de-la-verdad-para-desmentir-las-informaciones-sobre-el-ministerio-de-la-verdad/`),

    new BlogEntry(`Nace un perro verde`,
    `https://www.mediterraneodigital.com/images/2020/10/perro-verde.jpg`,
    `https://www.mediterraneodigital.com/humor/perro-verde`),

    new BlogEntry(`VÍDEO. ¡La foca que queremos! Detectan la primera foca pelirroja y de ojos azules de la historia`,
    `https://www.mediterraneodigital.com/images/2020/9/foca-albina.jpg`,
    `https://www.mediterraneodigital.com/humor/foca-albina`),

    new BlogEntry(`Rafa Nadal cierra un patrocinio con Tefal y a partir de ahora jugará al tenis con una sartén`,
    `https://emtstatic.com/2020/10/rafa-696x395.png`,
    `https://www.elmundotoday.com/2020/10/rafa-nadal-cierra-un-patrocinio-con-tefal-y-a-partir-de-ahora-jugara-al-tenis-con-una-sarten/`)
];