import { NgModule } from '@angular/core';
//RouterModule es el módulo donde está el sistema de rutas
//Routes es una declaración de un de tipo, que corresponde con un array de objetos Route
import { Routes, RouterModule } from '@angular/router';
//Importamos los modulos
import {LogginFormComponent} from './Components/loggin-form/loggin-form.component';
import {NewsListComponent} from './Components/news-list-c/news-list-c.component';

const routes: Routes = [
  { path: '', component: LogginFormComponent },
  { path: 'login', component: LogginFormComponent },
  { path: 'myNews', component: NewsListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
