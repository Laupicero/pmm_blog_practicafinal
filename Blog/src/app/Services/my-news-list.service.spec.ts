import { TestBed } from '@angular/core/testing';

import { MyNewsListService } from './my-news-list.service';

describe('MyNewsListService', () => {
  let service: MyNewsListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyNewsListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
