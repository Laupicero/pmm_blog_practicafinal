import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LogginService {

  constructor(private miRouter: Router) { }

  getRouter(){
    return this.miRouter;
  }
}
