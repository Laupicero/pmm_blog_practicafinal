import { TestBed } from '@angular/core/testing';

import { LogginService } from './loggin-serv.service';

describe('LogginServService', () => {
  let service: LogginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
