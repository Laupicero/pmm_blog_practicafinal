import { Injectable } from '@angular/core';
import {BlogEntry} from '../Models/BlogEntry';
import {myBlogNews_DAO} from '../Models/MyCustomBlogEntries';

@Injectable({
  providedIn: 'root'
})
export class MyNewsListService {
  private myNews: BlogEntry[] = myBlogNews_DAO;

  constructor() { }

  //Obtenemos la variable con todas las entradas
  getBlogEntries(): BlogEntry[]{
    return this.myNews;
  }


  addNewEntry(blogEntry: BlogEntry){
    this.myNews.unshift(blogEntry);
  }
}
