import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogginFormComponent } from './Components/loggin-form/loggin-form.component';
import { NewsListComponent } from './Components/news-list-c/news-list-c.component';
import { NewComponent } from './Components/new-c/new-c.component';

//Para los formularios, sino no funcionara
import { ReactiveFormsModule} from '@angular/forms';
import { NewFormComponent } from './Components/new-form/new-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LogginFormComponent,
    NewsListComponent,
    NewComponent,
    NewFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
