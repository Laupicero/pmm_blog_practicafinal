import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsListCComponent } from './news-list-c.component';

describe('NewsListCComponent', () => {
  let component: NewsListCComponent;
  let fixture: ComponentFixture<NewsListCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsListCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsListCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
