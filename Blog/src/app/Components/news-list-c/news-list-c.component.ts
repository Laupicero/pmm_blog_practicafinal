import { Component} from '@angular/core';
import {BlogEntry} from '../../Models/BlogEntry';
import {MyNewsListService} from '../../Services/my-news-list.service';
import {LogginService} from '../../Services/loggin-serv.service';

@Component({
  selector: 'app-news-list-c',
  templateUrl: './news-list-c.component.html',
  styleUrls: ['./news-list-c.component.css']
})
export class NewsListComponent{
  public blogNews: BlogEntry[];
  public showNewsFormcustomizable: boolean = false;

  constructor(private service: MyNewsListService, private serviceRouter: LogginService) { 
    this.blogNews = this.service.getBlogEntries();
  }

  //Vuelta al loggin
  returnToLoggin(){
    this.serviceRouter.getRouter().navigateByUrl('/login');
  }

  newsFormToggle(){
    this.showNewsFormcustomizable= !this.showNewsFormcustomizable;
  }

}
