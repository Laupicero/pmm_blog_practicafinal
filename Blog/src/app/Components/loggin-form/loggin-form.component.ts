import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {LogginService} from '../../Services/loggin-serv.service';

@Component({
  selector: 'logginForm-component',
  templateUrl: './loggin-form.component.html',
  styleUrls: ['./loggin-form.component.css']
})
export class LogginFormComponent{
  //Var para el form
  public userLogginForm: FormGroup;
  public userName: FormControl;
  public userPass: any;

  //Para cambiar el estado del input 
  //Bien: Fondo - blanco
  //Mal: Fondo - rojo
  public userStatus: String;
  public passwStatus: String;  

  //Para borrar la contraseña y el nombre por si ha habido errores. con ngModel / Deprecated
  // [(ngModel)]="passText"  - public passText: String = '';
  // [(ngModel)]="userText" - public userText: String = '';

  constructor(private loginServ: LogginService) {
    this.userName = new FormControl('', Validators.required);
    this.userPass = new FormControl('', Validators.required);

    this.userLogginForm = new FormGroup({
      userNameFCN: this.userName,
      userPassFCN: this.userPass
    });

    this.passwStatus = "rightStatus";
    this.userStatus = "rightStatus";
  }


  //Verificar estado de la contraseña
  verifyUser(userName: String, userPass: String){
    let passFormat: boolean = true;
    let userFormat: boolean = true;

    //Control nombre Usuario
    if(userName.length <= 0){
      this.userStatus = `wrongStatus`;
      userFormat = false;
      //Resetear valores input
      this.userName.reset();
      this.userPass.reset();
      alert("Please, write a right username");

    }else{
      this.userStatus = `rightStatus`;
    }

    //Control Contraseña
    if(userPass.length >= 0 && userPass.length <= 8){
      this.passwStatus = `wrongStatus`;
      passFormat = false;
      //Resetear valores input
      this.userName.reset();
      this.userPass.reset();
      alert(`Please, write a right password.\nMinimum '8' characters`);

    }else{
      this.passwStatus = `rightStatus`;
    }


    //Si todo esta correcto iremos a la página de las noticias a través del servicio
    if(passFormat && userFormat){
      this.loginServ.getRouter().navigateByUrl('/myNews');
    }
  }

}
