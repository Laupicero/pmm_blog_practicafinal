import { Component, Input, OnInit } from '@angular/core';
import {BlogEntry} from '../../Models/BlogEntry';

@Component({
  selector: 'newComponent',
  templateUrl: './new-c.component.html',
  styleUrls: ['./new-c.component.css']
})
export class NewComponent implements OnInit {
  @Input() newsItem: BlogEntry;

  ngOnInit(): void {
  }

}
