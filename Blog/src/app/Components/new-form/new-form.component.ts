import { Component} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {BlogEntry} from '../../Models/BlogEntry';
import {MyNewsListService} from '../../Services/my-news-list.service';

@Component({
  selector: 'news-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.css']
})
export class NewFormComponent{
  //Var para el formGroup
  formCustomizableNews: FormGroup;
  newsItemTitle: FormControl;
  newsItemLink: FormControl;
  newsItemImgURL: FormControl;


  //Constructor
  constructor(private service: MyNewsListService) { 

    this.formCustomizableNews = new FormGroup({
      titleForm: this.newsItemTitle = new FormControl('', Validators.required),
      imgLinkForm: this.newsItemLink = new FormControl('', Validators.required),
      imgURLForm: this.newsItemImgURL = new FormControl('', Validators.required)
    });
  }

  //Métodos

  //Llama al servicio para añadir una nueva Entrada
  callServiceForAddNewNewsItem(itemTitle: String, imgURL: String, link: String){

    if(itemTitle.length > 0 && imgURL.length >0 && link.length > 0){
      this.service.addNewEntry(new BlogEntry(itemTitle, imgURL, link));

    }else{
      alert(`ERROR!\n You must write in all fields`);
      
    }
      this.newsItemLink.reset();
      this.newsItemTitle.reset();
      this.newsItemImgURL.reset();
  }

}
